// eslint-disable-next-line
import css from './styles/main.css';
// eslint-disable-next-line
import styles from './styles/app.styl';

import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
